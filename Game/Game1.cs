using Data;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace ClientGame
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        UdpClient client;
        IPEndPoint clientEP;
        Socket send;
        IPEndPoint serverEP;
        int playerID;

        Texture2D player1Texture;
        Texture2D player2Texture;
        Vector2 player1Position;
        Vector2 player2Position;

        public Game1(string hostname, int serverReceivePort, int clientReceivePort, int playerID)
        {
            this.playerID = playerID;
            client = new UdpClient(clientReceivePort);
            clientEP = new IPEndPoint(IPAddress.Any, clientReceivePort);
            send = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            serverEP = new IPEndPoint(IPAddress.Parse(hostname), serverReceivePort);

            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            new Thread(new ThreadStart(ProcessIncoming)).Start();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            player1Texture = Content.Load<Texture2D>(@"Player.png");
            player2Texture = Content.Load<Texture2D>(@"Player.png");

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            MouseState mouseState = Mouse.GetState();
            player1Position.X = mouseState.X; player1Position.Y = mouseState.Y;
            SendReceiveUDP.Send(new PositionPacket(PacketType.POSITION, playerID, player1Position.X, player1Position.Y), serverEP, send);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();
            spriteBatch.Draw(player1Texture, player1Position, Color.White);
            spriteBatch.Draw(player2Texture, player2Position, Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }

        protected override void OnExiting(Object sender, EventArgs args)
        {
            base.OnExiting(sender, args);
            SendReceiveUDP.Send(new GameOverPacket(PacketType.GAMEOVER, true, playerID), serverEP, send);
            client.Close();
        }

        public void ProcessIncoming()
        {
            while(true)
            {
                Packet incoming = SendReceiveUDP.Receive(client, clientEP);
                if (incoming == null) { break; }
                else if (incoming.packetType == PacketType.POSITION)
                {
                    PositionPacket position = incoming as PositionPacket;
                    player2Position.X = position.X;
                    player2Position.Y = position.Y;
                }
                else if (incoming.packetType == PacketType.GAMEOVER)
                {
                    this.Exit();
                }
            }
        }
    }
}
