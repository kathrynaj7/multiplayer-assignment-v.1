#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
#endregion

namespace ClientGame
{
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            string hostname = args[0];
            int serverReceivePort = Convert.ToInt32(args[1]);
            int clientReceivePort = Convert.ToInt32(args[2]);
            int playerID = Convert.ToInt32(args[3]);
            using (var game = new Game1(hostname, serverReceivePort, clientReceivePort, playerID))
                game.Run();
        }
    }
}
