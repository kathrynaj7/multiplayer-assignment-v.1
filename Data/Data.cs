﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    [Serializable]
    public enum PacketType
    {
        BOOLEAN, CLIENTLIST, USERMESSAGE, SERVERMESSAGE, CLIENTNAME, REQUEST, GAME, GAMEPORT, POSITION, GAMEOVER
    };

    [Serializable]
    public class Packet
    {
        public PacketType packetType;

        public Packet()
        {

        }

        public Packet(PacketType packetType)
        {
            this.packetType = packetType;
        }
    }

    [Serializable]
    public class BooleanPacket : Packet 
    {
        public bool boolean;

        public BooleanPacket(PacketType packetType, bool boolean) : base (packetType)
        {
            this.boolean = boolean;
        }
    }

    [Serializable]
    public class ClientListPacket : Packet
    {
        public List<Player> clientList;

        public ClientListPacket(PacketType packetType, List<Player> clientList) : base (packetType)
        {
            this.clientList = clientList;
        }
    }

    [Serializable]
    public class UserMessagePacket : Packet
    {
        public string name;
        public string time;
        public string message;

        public UserMessagePacket(PacketType packetType, string name, string time, string message) : base(packetType)
        {
            this.name = name;
            this.time = time;
            this.message = message;
        }
    }

    [Serializable]
    public class ServerMessagePacket : Packet
    {
        public string time;
        public string message;

        public ServerMessagePacket(PacketType packetType, string time, string message) : base(packetType)
        {
            this.time = time;
            this.message = message;
        }
    }

    [Serializable]
    public class ClientNamePacket : Packet
    {
        public string name;

        public ClientNamePacket(PacketType packetType, string name): base(packetType)
        {
            this.name = name;
        }
    }

    [Serializable]
    public class RequestPacket : Packet
    {
        public string clientName;
        public string requestName;

        public RequestPacket(PacketType packetType, string clientName, string requestName) : base(packetType)
        {
            this.clientName = clientName;
            this.requestName = requestName;
        }
    }

    [Serializable]
    public class GamePacket : Packet
    {
        public bool boolean;
        public string opponent;
        public string instigator;

        public GamePacket(PacketType packetType, bool boolean, string opponent, string instigator) : base(packetType)
        {
            this.boolean = boolean;
            this.opponent = opponent;
            this.instigator = instigator;
        }
    }

    [Serializable]
    public class GamePortPacket : Packet
    {
        public int serverReceivePort;
        public int clientReceivePort;
        public int ID;

        public GamePortPacket(PacketType packetType, int serverReceivePort, int clientReceivePort, int ID) : base(packetType)
        {
            this.serverReceivePort = serverReceivePort;
            this.clientReceivePort = clientReceivePort;
            this.ID = ID;
        }
    }

    [Serializable]
    public class PositionPacket : Packet
    {
        public float X;
        public float Y;
        public int ID;

        public PositionPacket(PacketType packetType, int ID, float X, float Y) : base(packetType)
        {
            this.ID = ID;
            this.X = X;
            this.Y = Y;
        }
    }

    [Serializable]
    public class GameOverPacket : Packet
    {
        public bool gameOver;
        public int ID;

        public GameOverPacket(PacketType packetType, bool gameOver, int ID) : base(packetType)
        {
            this.gameOver = gameOver;
            this.ID = ID;
        }
    }

    [Serializable]
    public class Player
    {
        public string name;
        public bool inGame;
        [NonSerialized]
        public Socket socket;
        [NonSerialized]
        public IPAddress address;

        public Player() { }
        public Player(string name, Socket socket)
        {
            this.name = name;
            inGame = false;
            this.socket = socket;
            this.address = (socket.LocalEndPoint as IPEndPoint).Address;
        }
    }
}
