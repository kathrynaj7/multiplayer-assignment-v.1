﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public static class SendReceiveTCP
    {
        public static void Send(NetworkStream networkStream, Packet packet)
        {
            try
            {
                BinaryFormatter bf = new BinaryFormatter();
                MemoryStream ms = new MemoryStream();
                bf.Serialize(ms, packet);
                ms.WriteTo(networkStream);
                ms.SetLength(0);
            }
            catch (Exception) { }
        }
        public static Packet Receive(NetworkStream networkStream)
        {
            Packet packet = new Packet();
            try
            {
                BinaryFormatter bf = new BinaryFormatter();
                packet = (Packet)bf.Deserialize(networkStream);
                return packet;
            }
            catch (Exception) { return null; }
        }
    }
}
