﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public static class SendReceiveUDP
    {
        public static void Send(Packet packet, IPEndPoint ipEP, Socket socket)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(ms, packet);
                socket.SendTo(ms.ToArray(), ipEP);
            }
            catch (Exception) { }

        }

        public static Packet Receive(UdpClient client, IPEndPoint ipEP)
        {
            Packet packet = new Packet();
            try
            {
                byte[] receive = client.Receive(ref ipEP);
                MemoryStream ms = new MemoryStream(receive);
                ms.Seek(0, SeekOrigin.Begin);
                BinaryFormatter bf = new BinaryFormatter();
                packet = bf.Deserialize(ms) as Packet;
                return packet;
            }
            catch (Exception) { return null; }
        }
    }
}
