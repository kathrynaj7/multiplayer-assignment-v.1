﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Lobby
    {
        public List<Player> players;
        public List<Game> games;
        public Server server;

        public Lobby(Server server)
        {
            players = new List<Player>();
            games = new List<Game>();
            this.server = server;
        }

        public void HandleLobbyPacket(Packet incoming)
        {
            if (incoming.packetType == PacketType.USERMESSAGE)
            {
                SendUserMessage(incoming as UserMessagePacket);
            }
            if (incoming.packetType == PacketType.REQUEST)
            {
                SendGameRequest(incoming as RequestPacket);
            }
            if (incoming.packetType == PacketType.GAME)
            {
                SetupGame(incoming as GamePacket);
            }

        }

        public void RefreshClientList()
        {
            SendToAllPlayers(new ClientListPacket(PacketType.CLIENTLIST, players));

            Console.WriteLine("Active players: " + players.Count);
            Console.WriteLine("Players currently in the lobby:");
            foreach (Player player in players)
            {
                Console.WriteLine(player.name);
            }
            Console.WriteLine("\n");
        }

        public void SendUserMessage(UserMessagePacket messagePacket)
        {
            SendToAllPlayers(new UserMessagePacket(PacketType.USERMESSAGE, messagePacket.name, messagePacket.time, messagePacket.message));
        }

        public void SendServerMessage(string message)
        {
            SendToAllPlayers(new ServerMessagePacket(PacketType.SERVERMESSAGE, DateTime.Now.ToString("HH:mm"), message));
        }

        public void SendGameRequest(RequestPacket requestPacket)
        {
            SendToPlayer(new RequestPacket(PacketType.REQUEST, requestPacket.clientName, requestPacket.requestName), requestPacket.requestName);
        }

        public void SetupGame(GamePacket gamePacket)
        {
            Player opponent = new Player();
            Player instigator = new Player();
            SendToPlayer(new BooleanPacket(PacketType.BOOLEAN, true), gamePacket.instigator);
            foreach (Player player in players)
            {
                if (player.name == gamePacket.opponent)
                {
                    player.inGame = true;
                    opponent = player;
                }
                else if(player.name == gamePacket.instigator)
                {
                    player.inGame = true;
                    instigator = player;
                }
            }

            Game game = new Game(opponent, instigator);
            game.SendPorts(this);
            games.Add(game);

            RefreshClientList();
            SendServerMessage(opponent.name + " and " + instigator.name + " have entered a game.");
            Console.WriteLine("Game started between " + opponent.name + " and " + instigator.name);
        }

        public void CheckGameStatus()
        {
            foreach (Game game in games)
            {
                if (!game.active)
                {
                    games.Remove(game);
                }
            }
        }

        public void SendToPlayer(Packet message, string name)
        {
            foreach (Player player in players)
            {
                if (player.name == name)
                {
                    NetworkStream netStream = new NetworkStream(player.socket, true);
                    Data.SendReceiveTCP.Send(netStream, message);
                    break;
                }
            }
        }

        public void SendToAllPlayers(Packet message)
        {
            foreach (Player client in players)
            {
                NetworkStream netStream = new NetworkStream(client.socket, true);
                Data.SendReceiveTCP.Send(netStream, message);
            }
        }

        public bool CheckUserNameFree(string name)
        {
            foreach (Player player in players)
            {
                if (player.name == name) 
                    return false;
            }
            return true;
        }
    }
}
