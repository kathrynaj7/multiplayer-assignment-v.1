﻿using Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;

namespace Server
{
    public class Server
    {
        //Create TCPListener
        TcpListener myListener;
        Lobby lobby;
        public int port = 4444;

        public Server(bool UseLoopback, int port)
        {
            //Create new IPAddress
            IPAddress ipAddress = IPAddress.Parse("127.0.0.1");
            //Create new TCPListener with IPAddress and Port No.
            myListener = new TcpListener(ipAddress, port);
            lobby = new Lobby(this);
        }

        public void Start()
        {
            myListener.Start();
            Console.WriteLine("Listening...");
            Socket clientSocket;

            //Accept new socket, store in clientSocket. Start new thread.
            while (true)
            {
                try
                {
                    clientSocket = myListener.AcceptSocket();
                    NetworkStream netStream = new NetworkStream(clientSocket, true);
                    Player newPlayer;
                    string tempName = "";
                    bool acceptNewPlayer = false;
                    while (!acceptNewPlayer)
                    {
                        Packet incoming = Data.SendReceiveTCP.Receive(netStream);
                        if (incoming.packetType == PacketType.CLIENTNAME)
                        {
                            ClientNamePacket clientName = incoming as ClientNamePacket;
                            acceptNewPlayer = lobby.CheckUserNameFree(clientName.name);
                            if (acceptNewPlayer)
                            {
                                newPlayer = new Player(clientName.name, clientSocket);
                                tempName = clientName.name;
                                lobby.players.Add(newPlayer);
                                new Thread(new ParameterizedThreadStart(ServerReceiveFromClient)).Start(newPlayer);
                            }
                            Data.SendReceiveTCP.Send(netStream, new BooleanPacket(PacketType.BOOLEAN, acceptNewPlayer));
                        }
                    }

                    lobby.SendServerMessage(tempName + " has joined the lobby.");
                    Console.WriteLine("Client Added: " + tempName);

                    //Refresh client list
                    lobby.RefreshClientList();
                }

                catch (Exception e)
                {
                   Console.WriteLine("Accept failed");
                   Console.WriteLine(e);
                   break;
                }
            }
        }

        public void ServerReceiveFromClient(object obj)
        {
            Player player = obj as Player;
            NetworkStream netStream = new NetworkStream(player.socket, true);
            while (true)
            {
                try
                {
                    Packet incoming = Data.SendReceiveTCP.Receive(netStream);
                    lobby.HandleLobbyPacket(incoming);
                    //lobby.CheckGameStatus();
                }
                catch (Exception)
                {
                    for (int i = 0; i < lobby.players.Count; i++)
                    {
                        if (lobby.players[i].name == player.name)
                        {
                            lobby.players.RemoveAt(i);
                            i--;
                        }
                    }
                    
                    lobby.SendServerMessage(player.name + " has left the lobby.");
                    Console.WriteLine("Client Lost: " + player.name + "\n");
                    lobby.RefreshClientList();
                    break;
                }
            }
        }

        public void Stop()
        {
            myListener.Stop();
        }
    }
}