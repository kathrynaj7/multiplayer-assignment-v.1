﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    public class Game
    {
        public Lobby lobby;

        int serverReceivePort;
        int player1ReceivePort;
        int player2ReceivePort;
        Player opponent;
        Player instigator;

        UdpClient server;
        IPEndPoint serverEP;
        Socket send;

        IPEndPoint p1EP;
        IPEndPoint p2EP;

        public bool active = true;

        public Game(Player opponent, Player instigator)
        {
            this.opponent = opponent;
            this.instigator = instigator;
        }

        public void SendPorts(Lobby lobby)
        {
            active = true;
            this.lobby = lobby;
            int lobbyPort = lobby.server.port;

            int serverReceivePort = lobbyPort + 1;
            int player1ReceivePort = lobbyPort + 2;
            int player2ReceivePort = lobbyPort + 3;

            lobby.SendToPlayer(new GamePortPacket(PacketType.GAMEPORT, serverReceivePort, player1ReceivePort, 1), instigator.name);
            lobby.SendToPlayer(new GamePortPacket(PacketType.GAMEPORT, serverReceivePort, player2ReceivePort, 2), opponent.name);

            server = new UdpClient(serverReceivePort);
            serverEP = new IPEndPoint(IPAddress.Any, serverReceivePort);
            send = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            p1EP = new IPEndPoint(instigator.address, player1ReceivePort);
            p2EP = new IPEndPoint(opponent.address, player2ReceivePort);

            new Thread(new ThreadStart(ProcessGamePackets)).Start();
        }

        public void ProcessGamePackets()
        {
            while (active)
            {
                Packet incoming = SendReceiveUDP.Receive(server, serverEP);
                if (incoming.packetType == PacketType.POSITION)
                {
                    PositionPacket position = incoming as PositionPacket;
                    if (position.ID == 1)
                    {
                        SendReceiveUDP.Send(position, p2EP, send);
                    }
                    else
                    {
                        SendReceiveUDP.Send(position, p1EP, send);
                    }
                }
                else if (incoming.packetType == PacketType.GAMEOVER)
                {
                    GameOverPacket exitGame = incoming as GameOverPacket;
                    if (exitGame.gameOver)
                    {
                        if (exitGame.ID == 1)
                        {
                            SendReceiveUDP.Send(exitGame, p2EP, send);
                        }
                        else
                        {
                            SendReceiveUDP.Send(exitGame, p1EP, send);
                        }
                        lobby.SendToPlayer(new GameOverPacket(PacketType.GAMEOVER, true, 0), instigator.name);
                        lobby.SendToPlayer(new GameOverPacket(PacketType.GAMEOVER, true, 0), opponent.name);
                        foreach (Player player in lobby.players)
                        {
                            if (player.name == opponent.name)
                            {
                                player.inGame = false;
                            }
                            else if (player.name == instigator.name)
                            {
                                player.inGame = false;
                            }
                        }
                        server.Close();
                        lobby.RefreshClientList();
                        lobby.SendServerMessage(opponent.name + " and " + instigator.name + " have returned to the lobby.");
                        active = false;
                    }
                }
            }
        }
    }
}
