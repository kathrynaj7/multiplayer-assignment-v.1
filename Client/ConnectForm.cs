﻿using Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class ConnectForm : Form
    { 
        public ConnectForm()
        {
            InitializeComponent();
        }

        public string IP { get { return this.IPBox.Text; } }
        public string Port { get { return this.PortBox.Text; } }

        private void TestButton_Click(object sender, EventArgs e)
        {
            IPBox.Text = "127.0.0.1";
            PortBox.Text = "4444";
        }

    }
}
