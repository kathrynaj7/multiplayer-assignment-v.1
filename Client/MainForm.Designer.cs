﻿namespace Client
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.WelcomeLabel = new System.Windows.Forms.Label();
            this.NameLabel = new System.Windows.Forms.Label();
            this.ActivePlayers = new System.Windows.Forms.ListBox();
            this.ActivePlayersLabel = new System.Windows.Forms.Label();
            this.MessageBox = new System.Windows.Forms.ListBox();
            this.MessageSend = new System.Windows.Forms.TextBox();
            this.SendButton = new System.Windows.Forms.Button();
            this.StartGameButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // WelcomeLabel
            // 
            this.WelcomeLabel.AutoSize = true;
            this.WelcomeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WelcomeLabel.Location = new System.Drawing.Point(12, 9);
            this.WelcomeLabel.Name = "WelcomeLabel";
            this.WelcomeLabel.Size = new System.Drawing.Size(82, 20);
            this.WelcomeLabel.TabIndex = 0;
            this.WelcomeLabel.Text = "Welcome";
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameLabel.Location = new System.Drawing.Point(90, 9);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(0, 20);
            this.NameLabel.TabIndex = 1;
            // 
            // ActivePlayers
            // 
            this.ActivePlayers.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ActivePlayers.FormattingEnabled = true;
            this.ActivePlayers.ItemHeight = 16;
            this.ActivePlayers.Location = new System.Drawing.Point(16, 65);
            this.ActivePlayers.Name = "ActivePlayers";
            this.ActivePlayers.Size = new System.Drawing.Size(233, 260);
            this.ActivePlayers.Sorted = true;
            this.ActivePlayers.TabIndex = 2;
            this.ActivePlayers.TabStop = false;
            
            // 
            // ActivePlayersLabel
            // 
            this.ActivePlayersLabel.AutoSize = true;
            this.ActivePlayersLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ActivePlayersLabel.Location = new System.Drawing.Point(13, 46);
            this.ActivePlayersLabel.Name = "ActivePlayersLabel";
            this.ActivePlayersLabel.Size = new System.Drawing.Size(108, 16);
            this.ActivePlayersLabel.TabIndex = 3;
            this.ActivePlayersLabel.Text = "Active Players";
            // 
            // MessageBox
            // 
            this.MessageBox.BackColor = System.Drawing.SystemColors.Menu;
            this.MessageBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MessageBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MessageBox.FormattingEnabled = true;
            this.MessageBox.ItemHeight = 16;
            this.MessageBox.Location = new System.Drawing.Point(367, 12);
            this.MessageBox.Name = "MessageBox";
            this.MessageBox.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.MessageBox.Size = new System.Drawing.Size(306, 288);
            this.MessageBox.TabIndex = 4;
            this.MessageBox.TabStop = false;
            // 
            // MessageSend
            // 
            this.MessageSend.BackColor = System.Drawing.SystemColors.Menu;
            this.MessageSend.Location = new System.Drawing.Point(367, 305);
            this.MessageSend.Name = "MessageSend";
            this.MessageSend.Size = new System.Drawing.Size(264, 20);
            this.MessageSend.TabIndex = 5;
            // 
            // SendButton
            // 
            this.SendButton.BackColor = System.Drawing.SystemColors.Menu;
            this.SendButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SkyBlue;
            this.SendButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.SendButton.Location = new System.Drawing.Point(631, 304);
            this.SendButton.Name = "SendButton";
            this.SendButton.Size = new System.Drawing.Size(42, 22);
            this.SendButton.TabIndex = 6;
            this.SendButton.Text = "Send";
            this.SendButton.UseVisualStyleBackColor = false;
            this.SendButton.Click += new System.EventHandler(this.SendButton_Click);
            // 
            // StartGameButton
            // 
            this.StartGameButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.StartGameButton.Location = new System.Drawing.Point(94, 325);
            this.StartGameButton.Name = "StartGameButton";
            this.StartGameButton.Size = new System.Drawing.Size(75, 23);
            this.StartGameButton.TabIndex = 7;
            this.StartGameButton.Text = "Start Game";
            this.StartGameButton.UseVisualStyleBackColor = true;
            this.StartGameButton.Click += new System.EventHandler(this.StartGameButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(685, 348);
            this.Controls.Add(this.StartGameButton);
            this.Controls.Add(this.SendButton);
            this.Controls.Add(this.MessageSend);
            this.Controls.Add(this.MessageBox);
            this.Controls.Add(this.ActivePlayersLabel);
            this.Controls.Add(this.ActivePlayers);
            this.Controls.Add(this.NameLabel);
            this.Controls.Add(this.WelcomeLabel);
            this.Name = "MainForm";
            this.Text = "Lobby";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label WelcomeLabel;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.ListBox ActivePlayers;
        private System.Windows.Forms.Label ActivePlayersLabel;
        private System.Windows.Forms.ListBox MessageBox;
        private System.Windows.Forms.TextBox MessageSend;
        private System.Windows.Forms.Button SendButton;
        private System.Windows.Forms.Button StartGameButton;
    }
}

