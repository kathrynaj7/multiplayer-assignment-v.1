﻿using Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class MainForm : Form
    {
        private string hostname;
        private int port;

        private TcpClient tcpClient;
        private string name;
        private NetworkStream netStream;

        public MainForm()
        {
            tcpClient = new TcpClient();
            InitializeComponent();
        }

        public bool Connect()
        {
            ConnectForm connectForm = new ConnectForm();
            if (connectForm.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    hostname = connectForm.IP; port = Convert.ToInt32(connectForm.Port);
                    tcpClient.Connect(connectForm.IP, Convert.ToInt32(connectForm.Port));
                    netStream = tcpClient.GetStream();

                    StartupForm startForm = new StartupForm(tcpClient, netStream);
                    startForm.ShowDialog();
                    if (startForm.playerName == "") { }
                    else
                    {
                        name = startForm.playerName;
                        NameLabel.Text = name + "!";
                        Data.SendReceiveTCP.Send(netStream, new ClientNamePacket(PacketType.CLIENTNAME, name));
                    }
                    Run();
                    return true;
                }
                catch (Exception e)
                {
                    PopupForm popup = new PopupForm();
                    popup.TextBox.Text = "Exception: " + e.Message;
                    popup.ShowDialog();
                    return false;
                }
            }
            else
                return false;
        }

        public void Run()
        {
            try
            {
                Thread server = new Thread(new ThreadStart(ProcessServerResponse));
                server.IsBackground = true;
                server.Start();
            }
            catch (Exception) { }
        }

        private void ProcessServerResponse()
        {
            try
            {
                while (true)
                {
                    Packet incoming = Data.SendReceiveTCP.Receive(netStream);
                    string temp;
                    switch (incoming.packetType)
                    {
                        case PacketType.CLIENTLIST:
                            Invoke(new SetClientListDelegate(SetClientList), (incoming as ClientListPacket));
                            Application.DoEvents();
                            break;
                        case PacketType.USERMESSAGE:
                            UserMessagePacket message = incoming as UserMessagePacket;
                            temp = "[" + message.time + "] " + message.name + ": " + message.message;
                            Invoke(new AddMessageDelegate(AddMessage), temp);
                            break;
                        case PacketType.SERVERMESSAGE:
                            ServerMessagePacket serverMessage = incoming as ServerMessagePacket;
                            temp = "[" + serverMessage.time + "] " + serverMessage.message;
                            Invoke(new AddMessageDelegate(AddMessage), temp);
                            break;
                        case PacketType.REQUEST:
                            RequestPacket requestPacket = incoming as RequestPacket;
                            RequestForm request = new RequestForm();
                            request.Text = requestPacket.clientName + " wants to Play!";
                            request.TextBox.Text = "Accept and play with " + requestPacket.clientName + "?";
                            DialogResult requestAnswer = request.ShowDialog();
                            if(requestAnswer == DialogResult.Yes)
                            {
                                Data.SendReceiveTCP.Send(netStream, new GamePacket(PacketType.GAME, true, name, requestPacket.clientName));
                                Invoke(new GameStartUpDelegate(GameStartUp));
                            }
                            break;
                        case PacketType.BOOLEAN:
                            BooleanPacket boolPacket = incoming as BooleanPacket;
                            if(boolPacket.boolean)
                            {
                                Invoke(new GameStartUpDelegate(GameStartUp));
                            }
                            break;
                        case PacketType.GAMEOVER:
                            GameOverPacket exitGame = incoming as GameOverPacket;
                            if (exitGame.gameOver)
                            {
                                PopupForm popup = new PopupForm();
                                popup.TextBox.Text = "Game has ended.";
                                popup.ShowDialog();
                                Invoke(new ActivatePlayersDelegate(ActivatePlayers));
                                //SendReceiveTCP.Send(netStream, new GameOverPacket(PacketType.GAMEOVER, true, 0));
                            }
                            break;
                    }
                }
            }
            catch(Exception)
            {
                PopupForm popup = new PopupForm();
                popup.TextBox.Text = "Connection to server has been lost.";
                popup.ShowDialog();
                Application.Exit();
            }
        }

        delegate void SetClientListDelegate(ClientListPacket clientList);
        private void SetClientList(ClientListPacket clientList)
        {
            ActivePlayers.Items.Clear();
            List<string> names = new List<string>();
            for (int i = 0; i < clientList.clientList.Count; i++)
            {
                if (clientList.clientList[i].name != name && !clientList.clientList[i].inGame)
                {
                    names.Add(clientList.clientList[i].name);
                }
            }
            ActivePlayers.Items.AddRange(names.ToArray());
        }

        delegate void AddMessageDelegate(string message);
        private void AddMessage(string message)
        {
            MessageBox.Items.Add(message);
        }

        delegate void GameStartUpDelegate();
        private void GameStartUp()
        {
            Packet incoming = Data.SendReceiveTCP.Receive(netStream);
            while (incoming.packetType != PacketType.GAMEPORT)
            {
                incoming = Data.SendReceiveTCP.Receive(netStream);
            }
            if(incoming.packetType == PacketType.GAMEPORT)
            {
                GamePortPacket ports = incoming as GamePortPacket;
                Process game = new Process();
                game.StartInfo.FileName = "Game.exe";
                game.StartInfo.Arguments = hostname + " " + ports.serverReceivePort.ToString() + " " + ports.clientReceivePort.ToString() + " " + ports.ID.ToString();

                ActivePlayers.Enabled = false;
                
                game.Start();
            }
        }

        delegate void ActivatePlayersDelegate();
        private void ActivatePlayers()
        {
            ActivePlayers.Enabled = true;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void SendButton_Click(object sender, EventArgs e)
        {
            if (MessageSend.Text != "")
            {
                string time = DateTime.Now.ToString("HH:mm");
                string message = MessageSend.Text;
                Data.SendReceiveTCP.Send(netStream, new UserMessagePacket(PacketType.USERMESSAGE, name, time, message));
            }
            MessageSend.Text = "";
        }

        private void StartGameButton_Click(object sender, EventArgs e)
        {
            if (ActivePlayers.SelectedItem != null)
                Data.SendReceiveTCP.Send(netStream, new RequestPacket(PacketType.REQUEST, name, ActivePlayers.SelectedItem.ToString()));
        }
    }
}