﻿using Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class StartupForm : Form
    {
        TcpClient tcpClient;
        NetworkStream netStream;

        public StartupForm(TcpClient tcpClient, NetworkStream netStream)
        {
            this.tcpClient = tcpClient;
            this.netStream = netStream;
            InitializeComponent();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (nameBox.Text.Length == 0)
            {
                PopupForm popup = new PopupForm();
                popup.TextBox.Text = "Please enter a valid username.";
                popup.ShowDialog();
            }
            else
            {
                Data.SendReceiveTCP.Send(netStream, new ClientNamePacket(PacketType.CLIENTNAME, nameBox.Text));

                Packet incoming = Data.SendReceiveTCP.Receive(netStream);
                if (incoming.packetType == PacketType.BOOLEAN)
                {
                    BooleanPacket booleanPacket = incoming as BooleanPacket;
                    if (!booleanPacket.boolean)
                    {
                        PopupForm popup = new PopupForm();
                        popup.TextBox.Text = "Username is not available.\nPlease pick a different username.";
                        popup.ShowDialog();
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
        }

        public string playerName { get { return this.nameBox.Text; } }
    }
}
